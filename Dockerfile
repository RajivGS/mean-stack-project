FROM node:10.23.2-alpine As builder

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run build --prod

FROM nginx:1.19.6-alpine

COPY --from=builder /usr/src/app/dist/frontend/ /usr/share/nginx/html
